import ParkingLotManager.Interfaces.EntityInterface;
import ParkingLotManager.BlackList;
import ParkingLotManager.Log;
import ParkingLotManager.ParkingLot;
import ParkingLotManager.QueueGenerator;
import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
    	
	    	ParkingLot parking = new ParkingLot();
	    	DecimalFormat df = new DecimalFormat();
	    	df.setMaximumFractionDigits(2);
	    	
	    	//[BLACK LIST]//
	    	BlackList.addToBlackList("DLX12345");
	    	BlackList.addToBlackList("DLX34251");
	    	BlackList.addToBlackList("DLX54321");
	    	
	    	while(ParkingLot.getMaxHour() > ParkingLot.getHour()) {	
	    		
	    		if(ParkingLot.getHour() >= 6 && ParkingLot.getHour() <= 22)	{
	    			//[SET UP AMOUNT OF VEHICLES/PEDESTRIANS]//
			    	QueueGenerator.setBicyclesCount( QueueGenerator.getRandomNumber(0, 6) );
			    	QueueGenerator.setCarsCount( QueueGenerator.getRandomNumber(0, 20) );
			    	QueueGenerator.setTeacherCarsCount( QueueGenerator.getRandomNumber(0, 5) );
			    	QueueGenerator.setAnonymousPedestriansCount( QueueGenerator.getRandomNumber(0, 40) );
			    	QueueGenerator.setPedestriansCount( QueueGenerator.getRandomNumber(0, 40) );
			    	QueueGenerator.setDeliveryTruckCount( QueueGenerator.getRandomNumber(0, 3) );
			    	QueueGenerator.setEmergencyVehicleCount( QueueGenerator.getRandomNumber(0, 1) );
			    	QueueGenerator.setTankCount( QueueGenerator.getRandomNumber(0, 1) );
			        ArrayList<EntityInterface> queue = QueueGenerator.generate();
			        //
			        if(ParkingLot.getHour() == 6) {
			        	Log.info("There's " + parking.countCars() + " cars in the parking lot");
				        Log.info("There's " + parking.countBicycles() + " bicycles in the parking lot");
			        }
			        Log.info();
			        //[LETTING IN]//
			        for (EntityInterface entity : queue) {
			        	//[PAYING]//
			            if(parking.checkIfCanEnter(entity) && entity.haveToPark() && entity.haveToPay() ) {
			            	if(entity instanceof Car && parking.checkIfEnoughCarSpots() && !BlackList.checkIfInBlackList(entity.getPlate() ) ) { 	 
			            			parking.letIn(entity);
				            		parking.setCarsAmount(1);
				            		if(entity.getTicket().equals("Discount")) parking.payDiscountTicket();
				            		if(entity.getTicket().equals("Normal")) parking.payNormalTicket();
				            		if(entity.getTicket().equals("Overnight")) parking.payOvernightTicket();
			            	}
			            }
			            //[NON-PAYING]//
			            else if(parking.checkIfCanEnter(entity) && entity.haveToPark() && !entity.haveToPay() ) {
			            	if(entity instanceof Car 
			            			 
			            			&& parking.checkIfEnoughCarSpots() 
			            			&& !BlackList.checkIfInBlackList(entity.getPlate() ) ) { 	
			            		parking.letIn(entity);
			            		parking.setCarsAmount(1);
			            	}
			            	else if(entity instanceof Bicycle 
			            			&& parking.checkIfEnoughBicycleSpots() ) { 	
				                parking.letIn(entity);
				                parking.setBicycleAmount(1);
				            }
			            }
			            //[OTHER]//
			            else if(parking.checkIfCanEnter(entity) && !entity.haveToPark()) {
			            	parking.letIn(entity);
			            }   
			        }	
			        //[LETTING OUT OVER TIME]//
			        for(EntityInterface entity : parking.getEntitiesOnProperty()) {
				    	if(entity.haveToPay()){
				    		if((entity.getEnterHour() + 3) <= ParkingLot.getHour() && !entity.getTicket().equals("Overnight"))	parking.letOut(entity);
				    	}
				    	else if(!entity.haveToPay()) {
				    		if((entity.getEnterHour() + 3) <= ParkingLot.getHour())	parking.letOut(entity);
				    	}
			    	}
			        parking.letOutDelete();
			
			        Log.info();
			        Log.info("There's " + parking.countCars() + " cars in the parking lot");
			        Log.info("There's " + parking.countBicycles() + " bicycles in the parking lot");
	    		}//It's time from 6 to 22
	    		
	    		//[LETTING OUT EVERYONE WITHOUT OVERNIGHT TICKET]//
		    	if(ParkingLot.getHour() == 23) {
		    		for(EntityInterface entity : parking.getEntitiesOnProperty()) {
			    		if(entity.haveToPay() && !entity.getTicket().equals("Overnight"))	parking.letOut(entity);
			    		else if(!entity.haveToPay())	parking.letOut(entity);
			    	}
		    		Log.info();
			    	Log.info("There's " + parking.countCars() + " cars in the parking lot");
			        Log.info("There's " + parking.countBicycles() + " bicycles in the parking lot");
		    	}
		    	//[TIME PASSES]//
	    		ParkingLot.setHour(1);
	    	}
	    	
	    	//[END OF DAY]//
	        Log.info("There's " + df.format(parking.getCashRegistry()) + " zl in cash register");
    	}
}
