package Zoo.Animals;

public class Chimp extends Animal {

    public Chimp(String name) {
        super(name);
    }

    String[] getDiet() {
        return new String[]{"drinkable", "meat","vegatables","fruits"};
    }

}
