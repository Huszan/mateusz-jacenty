package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.DeliveryTruck;
import ParkingLotManager.Entities.EmergencyVehicle;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class QueueGenerator {

	private static int BICYCLES_COUNT;
	private static int ANONYMOUS_PEDESTRIANS_COUNT;
    private static int PEDESTRIANS_COUNT;
    private static int CARS_COUNT;
    private static int TEACHER_CARS_COUNT;
    private static int DELIVERY_TRUCK_COUNT;
    private static int EMERGENCY_VEHICLE_COUNT;
    private static int TANK_COUNT;

    public static ArrayList<EntityInterface> generate() {
        ArrayList<EntityInterface> queue = new ArrayList<>();

        for (int i = 0; i < ANONYMOUS_PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian(null,ParkingLot.getHour()));
        }

        for (int i = 0; i < PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian(getRandomName(),ParkingLot.getHour()));
        }

        for (int i = 0; i < CARS_COUNT; i++) {
            queue.add(new Car(getRandomPlateNumber(),ParkingLot.getHour(), getRandomTicket()));
        }

        for (int i = 0; i < TEACHER_CARS_COUNT; i++) {
            queue.add(new TeacherCar(getRandomPlateNumber(),ParkingLot.getHour(), getRandomTicket()));
        }

        for (int i = 0; i < BICYCLES_COUNT; i++) {
            queue.add(new Bicycle(ParkingLot.getHour()));
        }
        
        for (int i = 0; i < DELIVERY_TRUCK_COUNT; i++) {
            queue.add(new DeliveryTruck(getRandomPlateNumber(),ParkingLot.getHour()));
        }
        
        for (int i = 0; i < EMERGENCY_VEHICLE_COUNT; i++) {
            queue.add(new EmergencyVehicle(getRandomPlateNumber(),ParkingLot.getHour()));
        }
        
        for (int i = 0; i < TANK_COUNT; i++) {
            queue.add(new Tank(getRandomPlateNumber(),ParkingLot.getHour(), getRandomTicket()));
        }

        Collections.shuffle(queue);

        return queue;
    }

    private static String getRandomPlateNumber() {
        Random generator = new Random();
        return "DLX " + (generator.nextInt(89999) + 10000);
    }

    private static String getRandomName() {
        String[] names = {"John", "Jack", "James", "George", "Joe", "Jim"};
        return names[(int) (Math.random() * names.length)];
    }
    
    private static String getRandomTicket() {
    	String[] tickets = {"Discount","Normal","Overnight"};
    	return tickets[(int) (Math.random() * tickets.length)];
    }
    
    public static int setBicyclesCount(int BICYCLES_COUNT)
    {
    	return QueueGenerator.BICYCLES_COUNT = BICYCLES_COUNT;
    }
    
    public static int setAnonymousPedestriansCount(int ANONYMOUS_PEDESTRIANS_COUNT)
    {
    	return QueueGenerator.ANONYMOUS_PEDESTRIANS_COUNT = ANONYMOUS_PEDESTRIANS_COUNT;
    }
    
    public static int setPedestriansCount(int PEDESTRIANS_COUNT)
    {
    	return QueueGenerator.PEDESTRIANS_COUNT = PEDESTRIANS_COUNT;
    }
    
    public static int setCarsCount(int CARS_COUNT)
    {
    	return QueueGenerator.CARS_COUNT = CARS_COUNT;
    }
    
    public static int setTeacherCarsCount(int TEACHER_CARS_COUNT)
    {
    	return QueueGenerator.TEACHER_CARS_COUNT = TEACHER_CARS_COUNT;
    }
    
    public static int setDeliveryTruckCount(int DELIVERY_TRUCK_COUNT)
    {
    	return QueueGenerator.DELIVERY_TRUCK_COUNT = DELIVERY_TRUCK_COUNT;
    }
    
    public static int setEmergencyVehicleCount(int EMERGENCY_VEHICLE_COUNT)
    {
    	return QueueGenerator.EMERGENCY_VEHICLE_COUNT = EMERGENCY_VEHICLE_COUNT;
    }
    
    public static int setTankCount(int TANK_COUNT)
    {
    	return QueueGenerator.TANK_COUNT = TANK_COUNT;
    }
    
    public static int getRandomNumber(int MIN, int MAX) {
        Random generator = new Random();
        return (generator.nextInt(MAX-MIN) - MIN + 1 );
    }
    
}
