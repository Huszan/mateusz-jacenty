package Zad3;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class WinnerWasCalled extends Exception {
}

class Log {

	public static void info() {
		System.out.println("");
	}

	public static void info(String message) {
		System.out.println(message);
	}

}

class Dice {

	public int dicePoints = 6;
	
	public int changeDice(int choice)
	{
		dicePoints = choice;
		return dicePoints;
	}
	public int roll() {
		Random rand = new Random();
		int result = rand.nextInt(dicePoints) + 1;

		Log.info("Dice roll: " + result);
		return result;
	}

}

class Pawn {

	public int position;
	public String name;

	public Pawn(String name) {
		this.position = 0;
		this.name = name;
		
		Log.info(this.name + " joined the game.");
	}

}

class Board {

	public static int max_position = 100;
	
	public int max_rounds;
	public ArrayList<Pawn> pawns;
	public Dice dice;
	public Pawn winner;
	public int turnsCounter;
	Scanner scan = new Scanner(System.in); 

	public void chooseDice()
	{
		
		int chosenNumber;
		System.out.println("How many walls should your dice have?\nViable options = 4,6,8,12,24");
		do
			chosenNumber = scan.nextInt();
		while(chosenNumber != 4 && chosenNumber != 6 && chosenNumber != 8 && chosenNumber != 12 && chosenNumber != 24);
		
		dice.changeDice(chosenNumber);
	}
	
	public void chooseBoardSize()
	{
		int chosenNumber;
		System.out.println("How long should your board be?\nMax - 1000");
		do
			chosenNumber = scan.nextInt();
		while(chosenNumber > 1000 && chosenNumber <= 0);
		
		max_position = chosenNumber;
	}
	
	public void chooseMaxRounds()
	{
		int chosenNumber;
		System.out.println("How long should your game be? If you choose 0 game will not have limits\nMax - 300");
		do
			chosenNumber = scan.nextInt();
		while(chosenNumber > 300 && chosenNumber < 0);
		
		max_rounds = chosenNumber;
	}
	
	public Board() {
		this.pawns = new ArrayList<Pawn>();
		this.dice = null;
		this.winner = null;
		this.turnsCounter = 0;
	}

	public void performTurn() throws WinnerWasCalled {
		this.turnsCounter++;
		Log.info();
		Log.info("Turn " + this.turnsCounter);
		//Limitless game
		if(max_rounds == 0)
		{
			for(Pawn pawn : this.pawns) {
				int rollResult = this.dice.roll();
				pawn.position += rollResult;
				Log.info(pawn.name + " new position: " + pawn.position);
	
				if(pawn.position >= Board.max_position) {
					this.winner = pawn;
					throw new WinnerWasCalled();
				}
			}
		}
		//Limited game
		if(max_rounds > 0)
		{
			for(Pawn pawn : this.pawns) {
				int rollResult = this.dice.roll();
				pawn.position += rollResult;
				Log.info(pawn.name + " new position: " + pawn.position);
	
				if(pawn.position >= Board.max_position) {
					this.winner = pawn;
					throw new WinnerWasCalled();
				}
			}
			if(this.turnsCounter >= max_rounds)
			{
				int best=0;
				for(Pawn pawn : this.pawns) {
				if(pawn.position > best) 
					{
					best = pawn.position;
					this.winner = pawn;
					}
				}
				throw new WinnerWasCalled();
			}
		}
	}

}

class rollPawn
{
	public int randomize(int min, int max)
	{
		Random rand = new Random();
		int randomizedNumber = rand.nextInt(max)+min;
		return randomizedNumber;
	}
	public String nameRoll(int randomNumber)
	{
		String name=null;
		switch(randomNumber)
		{
		case 1:	name="Luke"; break;
		case 2: name="Martin"; break;
		case 3: name="Bob"; break;	
		case 4: name="Franklin"; break;
		case 5: name="Matt"; break;
		case 6: name="Rob"; break;
		case 7: name="Nancy"; break;
		case 8: name="Veronica"; break;
		case 9: name="Will"; break;
		case 10: name="Thomas"; break;
		}
		return name;
	}
	public String surnameRoll(int randomNumber)
	{
		String surname=null;
		switch(randomNumber)
		{
		case 1:	surname="Skywalker"; break;
		case 2: surname="Vader"; break;
		case 3: surname="Smith"; break;
		case 4: surname="Jones"; break;
		case 5: surname="Brown"; break;
		case 6: surname="Gray"; break;
		case 7: surname="Jonhson"; break;
		case 8: surname="Artist"; break;
		case 9: surname="Gore"; break;
		case 10: surname="Rosewell"; break;
		}
		return surname;
	}	
	public String merge(String s1, String s2)	{return s1 + " " + s2;}
}

public class Main {

	public static void main(String[] args) {
		Board board = new Board();
		board.dice = new Dice();
		rollPawn roll = new rollPawn();
		//Rolling random player names
		for(int i=0; i<5; i++)
		{
			board.pawns.add(new Pawn(roll.merge(roll.nameRoll(roll.randomize(1, 10)),roll.surnameRoll(roll.randomize(1, 10)))));
		}
		//Asking for dice change, board size and how long game will be
		board.chooseDice();
		board.chooseBoardSize();
		board.chooseMaxRounds();
		
		try {
			while(true) {
				board.performTurn();
			}
		} catch(WinnerWasCalled exception) {
			Log.info();
			Log.info(board.winner.name + " won.");
		}
	}

}