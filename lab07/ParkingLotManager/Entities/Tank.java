package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class Tank implements EntityInterface {
	
	int enterHour;
	private String ticket;
	private String plate;
	
	public Tank(String plate, int enterHour, String ticket) {
        this.plate = plate;
        this.enterHour = enterHour;
        this.ticket = ticket;
    }
	
	public String identify() {
        return "Unknown tank";
    }

    public boolean canEnter() {
        return false;
    }
    
    public int getEnterHour()
    {
    	return enterHour;
    }

	public String getType() {
		return "Tank";
	}
	
	public boolean haveToPay() {
		return false;
	}

	public boolean haveToPark() {
		return false;
	}

	public String getPlate() {
		return plate;
	}

	public String getTicket() {
		return ticket;
	}

}
