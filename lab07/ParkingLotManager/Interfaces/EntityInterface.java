package ParkingLotManager.Interfaces;

public interface EntityInterface {

    String identify();
    String getPlate();
    String getType();
    String getTicket();
    int getEnterHour();
    boolean canEnter();
    boolean haveToPay();
    boolean haveToPark();


}
