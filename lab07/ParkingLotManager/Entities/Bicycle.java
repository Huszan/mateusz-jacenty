package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class Bicycle implements EntityInterface {

	int enterHour;
	
	public Bicycle(int enterHour) {
		this.enterHour = enterHour;
	}
	
    public String identify() {
        return "Unknown bicycle";
    }

    public boolean canEnter() {
        return true;
    }
    
    public int getEnterHour()
    {
    	return enterHour;
    }

	public String getType() {
		return "Bicycle";
	}
	
	public boolean haveToPay() {
		return false;
	}

	public boolean haveToPark() {
		return true;
	}

	public String getPlate() {
		return null;
	}

	public String getTicket() {
		return null;
	}

}
