package ParkingLotManager;

import java.util.ArrayList;

public class BlackList {
	
	 private static ArrayList<String> blackList = new ArrayList<>();
	 
	 public static void addToBlackList(String plate)
	 {
		 blackList.add(plate);
	 }
	 
	 public static boolean checkIfInBlackList(String plate)
	 {
		 return blackList.contains(plate);
	 }

}
