package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class Pedestrian implements EntityInterface {

    private String name = "";
    private int enterHour;

    public Pedestrian() {}

    public Pedestrian(String name,int enterHour) {
        this.name = name;
        this.enterHour = enterHour;
    }

    public String identify() {
        return !(name == null) ? name : "Unknown pedestrian";
    }

    public boolean canEnter() {
        return true;
    }
    
    public int getEnterHour()
    {
    	return enterHour;
    }

	public String getType() {
		return "Pedestrian";
	}
	
	public boolean haveToPay() {
		return false;
	}

	public boolean haveToPark() {
		return false;
	}
	
	public String getPlate() {
		return null;
	}

	public String getTicket() {
		return null;
	}

}
