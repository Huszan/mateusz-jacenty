import Zoo.Animals.Chimp;
import Zoo.Animals.Dolphin;
import Zoo.Animals.Elephant;
import Zoo.Animals.Giraffe;
import Zoo.Animals.Lion;
import Zoo.Animals.Penguin;
import Zoo.Animals.Porcupine;
import Zoo.Animals.Tiger;
import Zoo.Food.Drinkable;
import Zoo.Food.Fruit;
import Zoo.Food.Meat;
import Zoo.Food.Seafood;
import Zoo.Food.Vegetable;
import Timer.Time;
import Zoo.Zoo;

public class Main {

    public static void main(String[] args) {
        Zoo zoo = new Zoo("Zoo Legnica");
        
        zoo.addAnimal(new Lion("Simba"))
            .addAnimal(new Lion("Mufasa"))
            .addAnimal(new Elephant("Dumbo"))
        	.addAnimal(new Dolphin("Flipper"))
        	.addAnimal(new Chimp("Koko"))
	        .addAnimal(new Giraffe("Long Neck"))
	        .addAnimal(new Penguin("Happy"))
	        .addAnimal(new Porcupine("Weirdo"))
	        .addAnimal(new Tiger("Misty"));

        while(Time.getCurrent() < Time.getMax()) {
        	if(Time.getCurrent() == 6) 		zoo.feedAnimals(new Meat());
        	if(Time.getCurrent() == 8)		zoo.feedAnimals(new Vegetable());
        	if(Time.getCurrent() == 9)		zoo.feedAnimals(new Fruit());
        	if(Time.getCurrent() == 13)		zoo.feedAnimals(new Seafood());
        	if(Time.getCurrent() == 15)		zoo.feedAnimals(new Drinkable());
        	
        	Time.increase();
        }
        
    }
}
