package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class Car implements EntityInterface {

    protected String plate;
	private String ticket;
    private int enterHour;

    public Car(String plate, int enterHour, String ticket) {
        this.plate = plate;
        this.enterHour = enterHour;
        this.ticket = ticket;
    }

    public String identify() {
        return "Car with plate number " + plate;
    }

    public boolean canEnter() {
        return true;
    }
    
    public int getEnterHour()
    {
    	return enterHour;
    }
    
	public String getType() {
		return "Car";
	}

	public boolean haveToPay() {
		return true;
	}

	public boolean haveToPark() {
		return true;
	}

	public String getPlate() {
		return plate;
	}

	public String getTicket() {
		return ticket;
	}

}
