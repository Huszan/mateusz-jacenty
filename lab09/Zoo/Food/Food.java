package Zoo.Food;

public abstract class Food {
	
	protected String type;
	
	public Food() {
	}
	
	public String getType() {
		return type;
	}
	
}
