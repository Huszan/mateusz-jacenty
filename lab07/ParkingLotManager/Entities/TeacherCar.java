package ParkingLotManager.Entities;

public class TeacherCar extends Car {

    public TeacherCar(String plate, int enterHour, String ticket) {
        super(plate, enterHour, ticket);
    }
    
	public boolean haveToPay() {
		return false;
	}

}
