package Timer;

public class Time {
	
	private static final int hPerDay = 24;
	
	private static int current = 0;
	
	public static void increase() {
		current++;
	}
	public static int getCurrent() {
		return current;
	}
	public static int getMax() {
		return hPerDay;
	}

}
