package ParkingLotManager.Entities;

public class EmergencyVehicle extends Car {

    public EmergencyVehicle(String plate, int enterHour) {
        super(plate, enterHour, null);
    }
    
	public boolean haveToPay() {
		return false;
	}
	
	 public String identify() {
	        return "Emergency Vehicle with plate number " + plate;
	    }
	 
	 public boolean haveToPark()
	 {
		 return false;
	 }

}
