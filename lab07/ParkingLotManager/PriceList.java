package ParkingLotManager;

public class PriceList {
	
	private static float NORMAL = (float) 5.29;
	private static float OVERNIGHT = NORMAL + 10;
	private static float DISCOUNT = NORMAL - 3;
	
	public static float getNormal()
	{
		return NORMAL;
	}
	
	public static float getDiscount()
	{
		return DISCOUNT;
	}
	
	public static float getOvernight()
	{
		return OVERNIGHT;
	}
	
	public static void setPrice(float price)
	{
		if(price <= 3)	NORMAL = price; 
	}

}
