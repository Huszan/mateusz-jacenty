package Zad3;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class WinnerWasCalled extends Exception {
}


class Log {

	public static void info() {
		System.out.println("");
	}

	public static void info(String message) {
		System.out.println(message);
	}

}

class Dice {

	private int sidesNumber;
	
	public int changeDice(int choice)
	{
		sidesNumber = choice;
		return sidesNumber;
	}
	public int roll() {
		Random rand = new Random();
		int result = rand.nextInt(sidesNumber) + 1;

		Log.info("Dice roll: " + result);
		return result;
	}
	public int getSidesNumber()
	{
		return sidesNumber;
	}

}

class Pawn {

	private int position;
	private String name;
	private String surname;

	public Pawn(String[] names, String[] surnames) {
		this.position = 0;
		this.name = rollName(names);
		this.surname = rollSurname(surnames);
		Log.info(name + " " + surname + " joined the game.");
	}
	
	public String getName()	{
		return(name + " " + surname);
	}
	
	public String setName(String name)
	{
		return this.name = name;
	}
	
	public String setSurname(String surname)
	{
		return this.surname = surname;
	}
	
	public int getPosition()
	{
		return position;
	}
	
	public int setPosition(int position)
	{
		return this.position+=position;
	}
	
	public int randomizeNumber(int max)
	{
		Random rand = new Random();
		int randomizedNumber = rand.nextInt(max);
		return randomizedNumber;
	}
	
	public String rollName(String[] names)
	{
			return this.name = setName(names[randomizeNumber(10)]);
	}	
	
	public String rollSurname(String[] surnames)
	{
		return this.surname = setSurname(surnames[randomizeNumber(10)]);
	}
	
}

class Board {

	private static int max_position;
	
	private int max_rounds;
	public ArrayList<Pawn> pawns;
	public Dice dice;
	public Pawn winner;
	private int turnsCounter;
	Scanner scan = new Scanner(System.in); 
	
	public Board() {
		this.pawns = new ArrayList<Pawn>();
		this.dice = null;
		this.winner = null;
		this.turnsCounter = 0;
	}
	
	public void chooseDice()
	{
		
		int chosenNumber;
		System.out.println("How many walls should your dice have?\nViable options = 4,6,8,12,24");
		do
			chosenNumber = scan.nextInt();
		while(chosenNumber != 4 && chosenNumber != 6 && chosenNumber != 8 && chosenNumber != 12 && chosenNumber != 24);
		
		dice.changeDice(chosenNumber);
	}
	
	public void chooseBoardSize()
	{
		int chosenNumber;
		System.out.println("How long should your board be?\nMax - 1000");
		do
			chosenNumber = scan.nextInt();
		while(chosenNumber > 1000 && chosenNumber <= 0);
		
		max_position = chosenNumber;
	}
	
	public void chooseMaxRounds()
	{
		int chosenNumber;
		System.out.println("How long should your game be? If you choose 0 game will not have limits\nMax - 300");
		do
			chosenNumber = scan.nextInt();
		while(chosenNumber > 300 && chosenNumber < 0);
		
		max_rounds = chosenNumber;
	}

	public void performTurn() throws WinnerWasCalled {
		this.turnsCounter++;
		Log.info();
		Log.info("Turn " + this.turnsCounter);

			for(Pawn pawn : this.pawns) {
				int rollResult = this.dice.roll();
				if(rollResult == 1 && (pawn.getPosition()%2) != 0)
				{
					Log.info(pawn.getName() + " GETS UNLUCKY AND MOVES BACKWARDS");
					rollResult = this.dice.roll();
					pawn.setPosition(-rollResult);
					Log.info(pawn.getName() + " new position: " + pawn.getPosition());
				}
				else if(rollResult == this.dice.getSidesNumber() && (pawn.getPosition() % 7) == 0)
				{
					pawn.setPosition(rollResult);
					Log.info(pawn.getName() + " new position: " + pawn.getPosition() + ", and gets an additional turn.");
					rollResult = this.dice.roll();
					pawn.setPosition(rollResult);
					Log.info(pawn.getName() + " new position: " + pawn.getPosition());
				}
				else
				{
				pawn.setPosition(rollResult);
				Log.info(pawn.getName() + " new position: " + pawn.getPosition());
				}
				if(pawn.getPosition() >= Board.max_position) {
					this.winner = pawn;
					throw new WinnerWasCalled();
				}
			}
			if(this.turnsCounter >= max_rounds && max_rounds != 0)
			{
				int best=0;
				for(Pawn pawn : this.pawns) {
				if(pawn.getPosition() > best) 
					{
					best = pawn.getPosition();
					this.winner = pawn;
					}
				}
				throw new WinnerWasCalled();
			}
		}
	
	}




public class Main {

	public static void main(String[] args) {
		Board board = new Board();
		board.dice = new Dice();
		String[] names = new String[] {"Luke","Martin","Bob","Franklin","Matt","Rob","Nancy","Veronica","Will","Thomas"};
		String[] surnames = new String[] {"Skywalker","Vader","Smith","Jones","Brown","Gray","Johnson","Artist","Gore","Rosewell"};
		//Rolling random player names
		for(int i=0; i<5; i++)
		{
			board.pawns.add(new Pawn(names,surnames));
		}
		//Asking for dice change, board size and how long game will be
		board.chooseDice();
		board.chooseBoardSize();
		board.chooseMaxRounds();
		
		try {
			while(true) {
				board.performTurn();
			}
		} catch(WinnerWasCalled exception) {
			Log.info();
			Log.info(board.winner.getName() + " won.");
		}
	}

}