package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.ArrayList;

final public class ParkingLot {

    private ArrayList<EntityInterface> entitiesOnProperty = new ArrayList<>();
    private ArrayList<EntityInterface> _entitiesOnProperty = new ArrayList<>();
    private int carsOnProperty = 0;
    private int bicyclesOnProperty = 0;
    private float cashRegistry = 0;
    private static int hour = 1;
    private static final int maxHour = 24;
    private static final int maxParkingSpots = 240;
    private static final int maxBicycleSpots = 24;

    public void letIn(EntityInterface entity) {
        entitiesOnProperty.add(entity);
        Log.info(entity.identify() + " let in at " + entity.getEnterHour() + " o'clock");
    }
    
    public void letOut(EntityInterface entity) {
		if(entity instanceof Bicycle && entity.haveToPark()) setBicycleAmount(-1);
		if(entity instanceof Car && entity.haveToPark()) setCarsAmount(-1);
    	Log.info(entity.identify() + " let out at " + hour + " o'clock");
    	_entitiesOnProperty.add(entity);
    }
    
    public void letOutDelete() {
    	entitiesOnProperty.removeAll(_entitiesOnProperty);
    }

    public ArrayList<EntityInterface> getEntitiesOnProperty() {
		return entitiesOnProperty;
    }
  
    //[MONEY]//
    public float getCashRegistry()
    {
    	return cashRegistry;
    }
    public void payNormalTicket()
    {
    	cashRegistry += PriceList.getNormal();
    }
    public void payDiscountTicket()
    {
    	cashRegistry += PriceList.getDiscount();
    }
    public void payOvernightTicket()
    {
    	cashRegistry += PriceList.getOvernight();
    }
    //[TIME]//
    public static int getHour()
    {
    	return hour;
    }
    
    public static int setHour(int hour)
    {
    	return ParkingLot.hour += hour;
    }
    
    public static int getMaxHour()
    {
    	return maxHour;
    }
    //[PARKING]//   
    public void setCarsAmount(int carAmount)
    {
    	carsOnProperty += carAmount;
    }
    public boolean checkIfEnoughCarSpots()
    {
    	if(maxParkingSpots > carsOnProperty) return true;
    	else return false;
    }
    public int countCars() 
    {
        return carsOnProperty;
    }
    
    public void setBicycleAmount(int bicycleAmount)
    {
    	bicyclesOnProperty += bicycleAmount;
    }
    public boolean checkIfEnoughBicycleSpots()
    {
    	if(maxBicycleSpots > bicyclesOnProperty) return true;
    	else return false;
    }
    public int countBicycles()
    {
    	return bicyclesOnProperty;
    }

    public boolean checkIfCanEnter(EntityInterface entity) {
        return entity.canEnter();
    }

}
