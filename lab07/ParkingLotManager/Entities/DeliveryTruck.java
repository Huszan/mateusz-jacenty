package ParkingLotManager.Entities;

public class DeliveryTruck extends Car {

    public DeliveryTruck(String plate, int enterHour) {
        super(plate, enterHour, null);
    }
    
	public boolean haveToPay() {
		return false;
	}
	
	 public String identify() {
	        return "Delivery Truck with plate number " + plate;
	    }
	 
	 public boolean haveToPark()
	 {
		 return false;
	 }

}
