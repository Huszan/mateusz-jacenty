package Zoo;

import Zoo.Animals.Animal;
import Zoo.Food.Food;

import java.util.ArrayList;

import Timer.Time;

public final class Zoo {

    private ArrayList<Animal> animals = new ArrayList<>();
    private String name;

    public Zoo(String name) {
        this.name = name;
        Log.info(name + " created.");
    }

    public Zoo addAnimal(Animal animal) {
        animals.add(animal);
        Log.info(animal.getName() + " added to zoo.");
        return this;
    }

    public void feedAnimals(Food food) {
        Log.info();
        Log.info("Feeding started at " + Time.getCurrent());

        for(Animal animal : animals) {
            try {
                animal.feed(food.getType());
            } catch (Exception e) {
                Log.warning(e.getMessage());
            }
        }
    }

}
